#include "SPCrypto.h"

SPCrypto::SPCrypto(){

	cHelper = CryptoHelper();
}

void SPCrypto::setOutFlow(std::string inFile){

	std::ifstream inData(inFile, std::ios::in | std::ios::binary);

	char c = inData.get();

	while (inData.good()) {

		outFlow.push_back(c);
		c = inData.get();
	}

	inData.close();
	outBlocks = cHelper.splitByBlocks(outFlow);
	return;
}

void SPCrypto::getInFlow(std::string inFile){
	std::ifstream inData(inFile, std::ios::in | std::ios::binary);

	char c = inData.get();

	while (inData.good()) {
	
		inFlow.push_back(c);
		c = inData.get();
	}

	inData.close();

	inBlocks = cHelper.splitByBlocks(inFlow);
}

void SPCrypto::getDecFlow(std::string decFile) {

	for (auto curBlock = decBlocks.begin(); curBlock != decBlocks.end(); curBlock++) {

		if (decBlocks.size() >= 2 && curBlock == (decBlocks.end() - 1)) {

			unsigned lastBlockLen = cHelper.getLastBlockLen();
			auto tmpBlock = CryptoHelper::block_t();

			for (unsigned i = 0; i < lastBlockLen; i++)
				tmpBlock[i] = (*curBlock)[i];

			uint32_t outWord = (*curBlock).to_ulong();

			for (unsigned i = 0; i < lastBlockLen / 8; i++)
				decFlow.push_back((outWord >> (lastBlockLen - 8 * (1 + i))) & 0xFF);

		}
		else {

			uint32_t outWord = (*curBlock).to_ulong();
			size_t block_len = CryptoHelper::block_len;
			for (unsigned i = 0; i < block_len / 8; i++) {
				CryptoHelper::flowelem_t pushval = (outWord >> (block_len - 8 * (1 + i))) & 0xFF;
				decFlow.push_back(pushval);
			}

		}
	}

	std::ofstream decData(decFile, std::ios::out | std::ios::binary);

	for (auto elem : decFlow) {
	
		decData << elem;
	}

	decData.close();
}

void SPCrypto::getOutFlow(std::string outFile){

	for (auto curBlock = outBlocks.begin(); curBlock != outBlocks.end(); curBlock++) {
	
		if (outBlocks.size() >= 2 && curBlock == (outBlocks.end() - 1)) {
		
			unsigned lastBlockLen = cHelper.getLastBlockLen();
			auto tmpBlock = CryptoHelper::block_t();
			
			for (unsigned i = 0; i < lastBlockLen; i++)
				tmpBlock[i] = (*curBlock)[i];

			uint32_t outWord = (*curBlock).to_ulong();
			
			for (unsigned i = 0; i < lastBlockLen / 8; i++)
				outFlow.push_back((outWord >> (lastBlockLen - 8 * (1 + i))) & 0xFF);

		}
		else {
		
			uint32_t outWord = (*curBlock).to_ulong();
			for (unsigned i = 0; i < CryptoHelper::block_len / 8; i++) {
				size_t block_len = CryptoHelper::block_len;
				CryptoHelper::flowelem_t pushval = (outWord >> (block_len - 8 * (1 + i))) & 0xFF;
				outFlow.push_back(pushval);
			}

		}
	}

	std::ofstream outData(outFile, std::ios::out | std::ios::binary);

	for (auto elem : outFlow) {

		outData << elem;
	}

	outData.close();

}

void SPCrypto::encrypt(){


	for (auto curBlock = inBlocks.begin(); curBlock != inBlocks.end(); curBlock++) {

		if (inBlocks.size() >= 2 && curBlock == (inBlocks.end() - 2)) {
		
			const auto outTMP = encryptBlock(*curBlock);
			const size_t lastBlockLen = cHelper.getLastBlockLen();
			const int fitLen = CryptoHelper::block_len - lastBlockLen;
			auto curCryptoBlock = CryptoHelper::block_t();

			for (unsigned i = 0; i < lastBlockLen; i++) {

				curCryptoBlock[i] = (*(inBlocks.end() - 1))[i];
			}
			for (unsigned i = lastBlockLen; i < CryptoHelper::block_len; i++) {
			
				curCryptoBlock[i] = outTMP[i];
			}

			outBlocks.push_back(encryptBlock(curCryptoBlock));
			outBlocks.push_back(encryptBlock(*(inBlocks.end() - 2)));
			break;
		}
		else {
		
			outBlocks.push_back(encryptBlock(*curBlock));
		}
	}
}

void SPCrypto::decrypt(){

	for (auto curBlock = outBlocks.begin(); curBlock != outBlocks.end(); curBlock++) {

		if (outBlocks.size() >= 2 && curBlock == (outBlocks.end() - 2)) {

			const auto outTMP = decryptBlock(*curBlock);
			const size_t lastBlockLen = cHelper.getLastBlockLen();
			const int fitLen = CryptoHelper::block_len - lastBlockLen;
			auto curCryptoBlock = CryptoHelper::block_t();

			for (unsigned i = 0; i < lastBlockLen; i++) {

				curCryptoBlock[i] = (*(outBlocks.end() - 1))[i];
			}
			for (unsigned i = lastBlockLen; i < CryptoHelper::block_len; i++) {

				curCryptoBlock[i] = outTMP[i];
			}

			decBlocks.push_back(decryptBlock(curCryptoBlock));
			decBlocks.push_back(decryptBlock(*(outBlocks.end() - 2)));
			break;
		}
		else {

			decBlocks.push_back(decryptBlock(*curBlock));
		}
	}

}

bool SPCrypto::validate(){

	bool result = true;
	size_t minSize = inFlow.size();

	if (inFlow.size() != decFlow.size())
		result = false;

	for (size_t i = 0; i < minSize; i++) {
	
		if (inFlow.at(i) != decFlow.at(i)) {
			result = false;
		}
	}

	/*if (inBlocks.size() != decBlocks.size()) {
		result = false;
		minSize = std::min(inBlocks.size(), decBlocks.size());
	}

	for (size_t i = 0; i < minSize; i++) {
	
		if (inBlocks[i] != decBlocks[i]) {
		
			std::cout << "#" << i << std::endl;
			std::cout << inBlocks[i] << std::endl;
			std::cout << decBlocks[i] << std::endl;
			std::cout << std::endl;
			result = false;
		}
	}*/



	return result;
}

CryptoHelper::block_t SPCrypto::roundSigma(CryptoHelper::block_t inBlock, CryptoHelper::key_t roundKey){

	auto outBlock = inBlock;
	auto outSubBlocks = CryptoHelper::sub_blocks_t();
	
	outBlock ^= roundKey;
	
	auto subBlocks = cHelper.splitBySubBlocks(outBlock);

	for (auto elem : subBlocks) {
		auto shuffled = cHelper.shuffleWithS(elem);
		outSubBlocks.push_back(shuffled);
	}

	//outSubBlocks = subBlocks;
	outBlock = cHelper.catSubBlocks(outSubBlocks);
	
	outBlock = pShuffle(outBlock);

	return outBlock;
}

CryptoHelper::block_t SPCrypto::roundDeSigma(CryptoHelper::block_t inBlock, CryptoHelper::key_t roundKey){
	
	auto outBlock = inBlock;
	auto outSubBlocks = CryptoHelper::sub_blocks_t();

	outBlock = pDeShuffle(outBlock);

	auto subBlocks = cHelper.splitBySubBlocks(outBlock);

	for (auto elem : subBlocks) {
		auto shuffled = cHelper.deshuffleWithS(elem);
		outSubBlocks.push_back(shuffled);
	}

	//outSubBlocks = subBlocks;
	outBlock = cHelper.catSubBlocks(outSubBlocks);
	outBlock ^= roundKey;

	return outBlock;
}

CryptoHelper::block_t SPCrypto::pShuffle(CryptoHelper::block_t inBlock) {

	CryptoHelper::block_t outBlock;
	for (size_t i = 0; i < outBlock.size(); i++) {
		outBlock[i] = inBlock[cHelper.P(i)];
	}

	return outBlock;
}

CryptoHelper::block_t SPCrypto::pDeShuffle(CryptoHelper::block_t inBlock) {

	CryptoHelper::block_t outBlock;

	for (int i = 0; i < outBlock.size(); i++) {
		outBlock[i] = inBlock[cHelper.deP(i)];
	}

	return outBlock;
}
